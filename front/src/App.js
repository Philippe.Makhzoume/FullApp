import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Contact from './contact'
import { pause } from './utils.js'
import { Transition } from 'react-spring'
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';



class App extends Component {
  state = { contacts_list: [], error_message: "", isLoading:false };

  getContact = async id => {
    // check if we already have the contact
    const previous_contact = this.state.contacts_list.find(
      contact => contact.id === id
    );
    if (previous_contact) {
      return; // do nothing, no need to reload a contact we already have
    }
    this.setState({ isLoading:true })
    try {
      const response = await fetch(`http://localhost:8080/contacts/get/${id}`);
      await pause();
      const answer = await response.json();
      if (answer.success) {
        // add the user to the current list of contacts
        const contact = answer.result;
        const contacts_list = [...this.state.contacts_list, contact];
        this.setState({ contacts_list, isLoading:false  });
      } else {
        this.setState({ error_message: answer.message, isLoading:false  });
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading:false  });
    }
  };

  deleteContact = async id => {
    this.setState({ isLoading:true })
    try {
      const response = await fetch(
        `http://localhost:8080/contacts/delete/${id}`
      );
      await pause();
      const answer = await response.json();
      if (answer.success) {
        // remove the user from the current list of users
        const contacts_list = this.state.contacts_list.filter(
          contact => contact.id !== id
        );
        this.setState({ contacts_list, isLoading:false  });
      } else {
        this.setState({ error_message: answer.message, isLoading:false  });
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading:false  });
    }
  };

  updateContact = async (id, props) => {
    this.setState({ isLoading:true })
    try {
      if (!props || !(props.name || props.email)) {
        throw new Error(
          `you need at least name or email properties to update a contact`
        );
      }
      const response = await fetch(
        `http://localhost:8080/contacts/update/${id}?name=${props.name}&email=${
          props.email
        }`
      );
      await pause();
      const answer = await response.json();
      if (answer.success) {
        // we update the user, to reproduce the database changes:
        const contacts_list = this.state.contacts_list.map(contact => {
          // if this is the contact we need to change, update it. This will apply to exactly
          // one contact
          if (contact.id === id) {
            const new_contact = {
              id: contact.id,
              name: props.name || contact.name,
              email: props.name || contact.email
            };
            return new_contact;
          }
          // otherwise, don't change the contact at all
          else {
            return contact;
          }
        });
        this.setState({ contacts_list, isLoading:false });
      } else {
        this.setState({ error_message: answer.message, isLoading:false  });
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading:false  });
    }
  };
  createContact = async props => {
    this.setState({ isLoading:true })
    try {
      if (!props || !(props.name && props.email)) {
        throw new Error(
          `you need both name and email properties to create a contact`
        );
      }
      const { name, email } = props;
      const response = await fetch(
        `http://localhost:8080/contacts/new/?name=${name}&email=${email}`
      );
      await pause();
      const answer = await response.json();
      if (answer.success) {
        // we reproduce the user that was created in the database, locally
        const id = answer.result;
        const contact = { name, email, id };
        const contacts_list = [...this.state.contacts_list, contact];
        this.setState({ contacts_list, isLoading:false });
      } else {
        this.setState({ error_message: answer.message , isLoading:false});
      }
    } catch (err) {
      this.setState({ error_message: err.message , isLoading:false});
    }
  };

  getContactsList = async order => {
    this.setState({ isLoading:true })
    try {
      const response = await fetch(`http://localhost:8080/contacts/list?order=${order}`);
      await pause();
      const answer = await response.json();
      if (answer.success) {
        const contacts_list = answer.result;
        this.setState({ contacts_list, isLoading:false });
      } else {
        this.setState({ error_message: answer.message, isLoading: false });
      }
    } catch (err) {
      this.setState({ error_message: err.message, isLoading: false });
    }
  };

  componentDidMount() {
    this.getContactsList();
  }
  onSubmit = (evt) => {
    // stops the page from refreshing
    evt.preventDefault();
    // extract name and email from state
    const { name, email } = this.state
    // create the contact from mail and email
    this.createContact({name, email})
    // empty name and email so the text input fields are reset
    this.setState({name:"", email:""})
  }
  render() {
    const { contacts_list, error_message } = this.state;
    const { isLoading } = this.state;

    return ( 
    <div className = "App" >
    <form className="third" onSubmit={this.onSubmit}>
          <input
            type="text"
            placeholder="name"
            onChange={evt => this.setState({ name: evt.target.value })}
            value={this.state.name}
          />
          <input
            type="text"
            placeholder="email"
            onChange={evt => this.setState({ email: evt.target.value })}
            value={this.state.email}
          />
          <div>
            <input type="submit" value="ok" />
            <input type="reset" value="cancel" className="button" />
          </div>
        </form>

        <header className = "App-header">
        {isLoading ? <p>loading...</p>  : false }
        {error_message?<p>{error_message}</p>: false}
        <Transition
          items={contacts_list}
          keys={contact => contact.id}
          from={{ transform: "translate3d(-100px,0,0)" }}
          enter={{ transform: "translate3d(0,0px,0)" }}
          leave={{ transform: "translate3d(-100px,0,0)" }}
        >
          {contact => style => (
            <div style={style}>
              <Contact
                key={contact.id}
                id={contact.id}
                name={contact.name}
                email={contact.email}
                updateContact={this.updateContact}
                deleteContact={this.deleteContact}
                />
            </div>
          )}
        </Transition>
        <ToastContainer />
        </header>
      </div>
    );
  }
}

export default App;